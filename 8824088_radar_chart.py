
#-----Statement of Authorship----------------------------------------#
#
#  By submitting this task the signatories below agree that it
#  represents our own work and that we both contributed to it.  We
#  are aware of the University rule that a student must not
#  act in a manner which constitutes academic dishonesty as stated
#  and explained in QUT's Manual of Policies and Procedures,
#  Section C/5.3 "Academic Integrity" and Section E/2.1 "Student
#  Code of Conduct".
#
#  First student's no: n8824088
#  First student's name: Scott Schultz
#  Portfolio contribution: 100%
#
#  Second student's no: Solo Submission
#
#  Contribution percentages refer to the whole portfolio, not just this
#  task.  Percentage contributions should sum to 100%.  A 50/50 split is
#  NOT necessarily expected.  The percentages will not affect your marks
#  except in EXTREME cases.
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  RADAR CHART
#
#  Radar charts are a common way of visualising data comprising
#  several values of different types.  In this task you and your
#  programming partner will develop a Python function that uses
#  Turtle graphics to draw a radar chart given a list of data
#  values.  See the instruction sheet accompanying this file for
#  full details.
#
#--------------------------------------------------------------------#



#-----Students' Solution---------------------------------------------#
#
#  Complete the task by filling in the template below.

#--------------------------------------------------------------------#
#                               Imports                              #
#--------------------------------------------------------------------#

from turtle import *

#--------------------------------------------------------------------#
#                              Constants                             #
#--------------------------------------------------------------------#

# These constants control all aspects of the chart from line widths to the colours used. Go nuts and blind yourself with fabulous colours such as (255,0,255) or (0,255,0)

screen_size = 800
screen_border = 100
max_coord = screen_size / 2

# Base chart metrics (the spokes and such)
chart_line_width = 2
chart_line_length = (screen_size/2)-screen_border
chart_dot_size = 14

# Filled graph metrics (the shape created from the data_set)

graph_line_width = 2
graph_dot_size = 10

# [r,g,b] triplets
chart_line_colour = [50,50,50]
chart_dot_colour = [0,0,0]
chart_fill_colour = [100,100,100]

graph_line_colour = [50,100,50]
graph_dot_colour = [0,50,0]
graph_fill_colour = [0,150,0]


background_colour = [150,150,150]

# Fonts
font_family = "courier"
font_size = 20
font_weight = "bold"


def radar_chart(data_set):

	# Builds our co-ord list for the outside loop of our graph
	spoke_count = len(data_set)

	init_window()

	slice_angle = build_outside_ring(spoke_count)
	draw_base(spoke_count,slice_angle)
	data_coord_list = build_data_coords(data_set,slice_angle)
	draw_graph(data_coord_list)

def init_window():
	clear()
	colormode(255) # Set turtle to accept rgb 1...255 values for color
	setup(screen_size,screen_size)
	title('Radar Chart')
	bgcolor(background_colour)

def build_outside_ring(spoke_count):

	goto(0,0) # Start at the center of the window
	setheading(90) # Face the top of the screen
	pensize(chart_line_width) # Get the line width...
	color(chart_line_colour,chart_fill_colour) # ...and its colour

	slice_angle = 360/spoke_count # Calculate the slice size

	chart_coord_list = [] # initialise the list we'll need to draw the outer rim of the spokes
	for _ in range(spoke_count):
		forward(chart_line_length) # Draw a line based on the chart_line_length minus the border of the window
		chart_coord_list.append([xcor(),ycor()]) # add the current turtles location to the list
		goto(0,0) # Return to the center
		right(slice_angle) # Turn to the next angle

	begin_fill()
	for outer_coord in chart_coord_list:
		goto(outer_coord)
	goto(chart_coord_list[0])
	end_fill()

	return slice_angle

def draw_base(spoke_count,slice_angle):

	goto(0,0) # Start at the center of the window
	setheading(90) # Face the top of the screen
	pensize(chart_line_width) # Get the line width...
	color(chart_line_colour) # ...and its colour

	for spoke_number in range(spoke_count):
		forward(chart_line_length) # Draw a line based on the chart_line_length minus the border of the window
		dot(chart_dot_size,chart_dot_colour) # Draw a dot at the end point
		penup() # lift the pen, we don't want to redraw a line over the dot or draw a line to the spoke number

	# label offseting, without this the numbers at the top will have a greater distance from its respective dot
	# than the bottom half of the graph.
	# It's not perfect, but its better than going from the center. write is weird like that.
		goto(0,-font_size) # Return to the center offset by the fontsize
		forward(chart_line_length) # Draw a line based on the chart_line_length minus the border of the window
		forward(25) # move an additional 50% of the screen border to draw the spoke number
		write(spoke_number,True,align="center",font=(font_family,font_size,font_weight)) # write the current spoke number

		goto(0,0) # Return to the center
		pendown() # put the pen back down for the next loop
		right(slice_angle) # Turn to the next angle
		# print spoke_number*slice_angle


	return slice_angle

def build_data_coords(data_set,slice_angle):
	# print data_set
	# print slice_angle

	goto(0,0) # Marty, we have to go back!
	setheading(90) # Face the top of the screen
	penup() # We don't want to draw lines for this segment

	data_coords = [] # initialise the list
	for data_point in data_set:
		spoke_percent = float(data_point)/100
		forward(chart_line_length*spoke_percent) # move a distance based on each item in the data_set list
		data_coords.append([xcor(),ycor()]) # store the x,y coordinates of the turtle for this data item
		# print chart_line_length*spoke_percent # setheading(90) # Face the top of the screen
		#print "%d,%d" %(xcor(),ycor()) # setheading(90) # Face the top of the screen
		goto(0,0) # Return to the center
		right(slice_angle) # Turn to the next angle

	return data_coords # now return our new list of data coordinates

def draw_graph(data_coord_list):

	pensize(graph_line_width) # Get the line width...
	color(graph_line_colour,graph_fill_colour) # ...and its colour

	fill(True)
	for poly_coord in data_coord_list:
		goto(poly_coord)
		pendown()
	goto(data_coord_list[0]) # close the shape by going back to our first point
	fill(False)

	penup() # draw the dots
	for poly_coord in data_coord_list:
		goto(poly_coord)
		dot(graph_dot_size,graph_dot_colour) # Draw a dot at the end point
	goto(0,0)
	pendown()

#
#--------------------------------------------------------------------#



#-----Testing--------------------------------------------------------#
#
#  Below is some data to use when testing your function, including
#  the example shown in the instruction sheet.  When we mark your
#  solution we may use tests other than these, so make sure that
#  your function works for any given data set.
#

# Some "typical" test data
test1 = [32, 56, 100, 78, 23, 90, 0, 12, 45, 90, 23]
test2 = [88, 76, 63, 84, 98, 10, 17, 82, 65]
test3 = [32, 61, 15, 95, 24]
test4 = [60, 97, 86, 39, 67, 22, 62, 58, 20, 37, 65,
		 72, 34, 67, 64, 56, 50, 32, 50, 93, 68]

# Some "extreme" test data
test5 = [25, 75, 25] # a triangle (the shortest "reasonable" list) 
test6 = [100, 100, 100, 100] # produces a filled square

# Run a test - change the test number for different charts
speed('fastest') # draw quickly
hideturtle()
radar_chart(test1) # call the function
done()
#
#--------------------------------------------------------------------#


